EESchema Schematic File Version 4
LIBS:teensythumbboard-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Teensy Thumb Board"
Date "2018-11-11"
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L teensythumbboard-rescue:SW_Push SW1
U 1 1 5A91B5FA
P 5600 2100
F 0 "SW1" H 5650 2200 50  0000 L CNN
F 1 "SW_Left" H 5600 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5600 2300 50  0001 C CNN
F 3 "" H 5600 2300 50  0001 C CNN
	1    5600 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW2
U 1 1 5A91BAED
P 6150 2100
F 0 "SW2" H 6200 2200 50  0000 L CNN
F 1 "SW_UP" H 6150 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6150 2300 50  0001 C CNN
F 3 "" H 6150 2300 50  0001 C CNN
	1    6150 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW3
U 1 1 5A91BB18
P 6700 2100
F 0 "SW3" H 6750 2200 50  0000 L CNN
F 1 "SW_Down" H 6700 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6700 2300 50  0001 C CNN
F 3 "" H 6700 2300 50  0001 C CNN
	1    6700 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW4
U 1 1 5A91BBAB
P 7250 2100
F 0 "SW4" H 7300 2200 50  0000 L CNN
F 1 "SW_Right" H 7250 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7250 2300 50  0001 C CNN
F 3 "" H 7250 2300 50  0001 C CNN
	1    7250 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW5
U 1 1 5A91BC26
P 7800 2100
F 0 "SW5" H 7850 2200 50  0000 L CNN
F 1 "SW_0" H 7800 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7800 2300 50  0001 C CNN
F 3 "" H 7800 2300 50  0001 C CNN
	1    7800 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW6
U 1 1 5A91BCA7
P 8350 2100
F 0 "SW6" H 8400 2200 50  0000 L CNN
F 1 "SW_?" H 8350 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8350 2300 50  0001 C CNN
F 3 "" H 8350 2300 50  0001 C CNN
	1    8350 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW7
U 1 1 5A91BD68
P 8900 2100
F 0 "SW7" H 8950 2200 50  0000 L CNN
F 1 "SW_-" H 8900 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8900 2300 50  0001 C CNN
F 3 "" H 8900 2300 50  0001 C CNN
	1    8900 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW8
U 1 1 5A91BDE1
P 9450 2100
F 0 "SW8" H 9500 2200 50  0000 L CNN
F 1 "SW_+" H 9450 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9450 2300 50  0001 C CNN
F 3 "" H 9450 2300 50  0001 C CNN
	1    9450 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW9
U 1 1 5A91C44D
P 10000 2100
F 0 "SW9" H 10050 2200 50  0000 L CNN
F 1 "SW_;" H 10000 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10000 2300 50  0001 C CNN
F 3 "" H 10000 2300 50  0001 C CNN
	1    10000 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW10
U 1 1 5A91C49E
P 10550 2100
F 0 "SW10" H 10600 2200 50  0000 L CNN
F 1 "SW_'" H 10550 2040 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10550 2300 50  0001 C CNN
F 3 "" H 10550 2300 50  0001 C CNN
	1    10550 2100
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW11
U 1 1 5A91E250
P 5600 2550
F 0 "SW11" H 5650 2650 50  0000 L CNN
F 1 "SW_7" H 5600 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5600 2750 50  0001 C CNN
F 3 "" H 5600 2750 50  0001 C CNN
	1    5600 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW12
U 1 1 5A91E25C
P 6150 2550
F 0 "SW12" H 6200 2650 50  0000 L CNN
F 1 "SW_8" H 6150 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6150 2750 50  0001 C CNN
F 3 "" H 6150 2750 50  0001 C CNN
	1    6150 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW13
U 1 1 5A91E262
P 6700 2550
F 0 "SW13" H 6750 2650 50  0000 L CNN
F 1 "SW_9" H 6700 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6700 2750 50  0001 C CNN
F 3 "" H 6700 2750 50  0001 C CNN
	1    6700 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW14
U 1 1 5A91E274
P 7250 2550
F 0 "SW14" H 7300 2650 50  0000 L CNN
F 1 "SW_4" H 7250 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7250 2750 50  0001 C CNN
F 3 "" H 7250 2750 50  0001 C CNN
	1    7250 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW15
U 1 1 5A91E280
P 7800 2550
F 0 "SW15" H 7850 2650 50  0000 L CNN
F 1 "SW_5" H 7800 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7800 2750 50  0001 C CNN
F 3 "" H 7800 2750 50  0001 C CNN
	1    7800 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW16
U 1 1 5A91E28C
P 8350 2550
F 0 "SW16" H 8400 2650 50  0000 L CNN
F 1 "SW_6" H 8350 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8350 2750 50  0001 C CNN
F 3 "" H 8350 2750 50  0001 C CNN
	1    8350 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW17
U 1 1 5A91E298
P 8900 2550
F 0 "SW17" H 8950 2650 50  0000 L CNN
F 1 "SW_1" H 8900 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8900 2750 50  0001 C CNN
F 3 "" H 8900 2750 50  0001 C CNN
	1    8900 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW18
U 1 1 5A91E29E
P 9450 2550
F 0 "SW18" H 9500 2650 50  0000 L CNN
F 1 "SW_2" H 9450 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9450 2750 50  0001 C CNN
F 3 "" H 9450 2750 50  0001 C CNN
	1    9450 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW19
U 1 1 5A91E2A4
P 10000 2550
F 0 "SW19" H 10050 2650 50  0000 L CNN
F 1 "SW_3" H 10000 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10000 2750 50  0001 C CNN
F 3 "" H 10000 2750 50  0001 C CNN
	1    10000 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW20
U 1 1 5A91E2AA
P 10550 2550
F 0 "SW20" H 10600 2650 50  0000 L CNN
F 1 "SW_Esc" H 10550 2490 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10550 2750 50  0001 C CNN
F 3 "" H 10550 2750 50  0001 C CNN
	1    10550 2550
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW21
U 1 1 5A91EBE5
P 5600 3000
F 0 "SW21" H 5650 3100 50  0000 L CNN
F 1 "SW_Q" H 5600 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5600 3200 50  0001 C CNN
F 3 "" H 5600 3200 50  0001 C CNN
	1    5600 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW22
U 1 1 5A91EBF1
P 6150 3000
F 0 "SW22" H 6200 3100 50  0000 L CNN
F 1 "SW_W" H 6150 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6150 3200 50  0001 C CNN
F 3 "" H 6150 3200 50  0001 C CNN
	1    6150 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW23
U 1 1 5A91EBF7
P 6700 3000
F 0 "SW23" H 6750 3100 50  0000 L CNN
F 1 "SW_E" H 6700 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6700 3200 50  0001 C CNN
F 3 "" H 6700 3200 50  0001 C CNN
	1    6700 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW24
U 1 1 5A91EC09
P 7250 3000
F 0 "SW24" H 7300 3100 50  0000 L CNN
F 1 "SW_R" H 7250 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7250 3200 50  0001 C CNN
F 3 "" H 7250 3200 50  0001 C CNN
	1    7250 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW25
U 1 1 5A91EC15
P 7800 3000
F 0 "SW25" H 7850 3100 50  0000 L CNN
F 1 "SW_T" H 7800 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7800 3200 50  0001 C CNN
F 3 "" H 7800 3200 50  0001 C CNN
	1    7800 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW26
U 1 1 5A91EC21
P 8350 3000
F 0 "SW26" H 8400 3100 50  0000 L CNN
F 1 "SW_Y" H 8350 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8350 3200 50  0001 C CNN
F 3 "" H 8350 3200 50  0001 C CNN
	1    8350 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW27
U 1 1 5A91EC2D
P 8900 3000
F 0 "SW27" H 8950 3100 50  0000 L CNN
F 1 "SW_U" H 8900 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8900 3200 50  0001 C CNN
F 3 "" H 8900 3200 50  0001 C CNN
	1    8900 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW28
U 1 1 5A91EC33
P 9450 3000
F 0 "SW28" H 9500 3100 50  0000 L CNN
F 1 "SW_I" H 9450 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9450 3200 50  0001 C CNN
F 3 "" H 9450 3200 50  0001 C CNN
	1    9450 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW29
U 1 1 5A91EC39
P 10000 3000
F 0 "SW29" H 10050 3100 50  0000 L CNN
F 1 "SW_O" H 10000 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10000 3200 50  0001 C CNN
F 3 "" H 10000 3200 50  0001 C CNN
	1    10000 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW30
U 1 1 5A91EC3F
P 10550 3000
F 0 "SW30" H 10600 3100 50  0000 L CNN
F 1 "SW_P" H 10550 2940 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10550 3200 50  0001 C CNN
F 3 "" H 10550 3200 50  0001 C CNN
	1    10550 3000
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW31
U 1 1 5A91EC66
P 5600 3450
F 0 "SW31" H 5650 3550 50  0000 L CNN
F 1 "SW_A" H 5600 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5600 3650 50  0001 C CNN
F 3 "" H 5600 3650 50  0001 C CNN
	1    5600 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW32
U 1 1 5A91EC72
P 6150 3450
F 0 "SW32" H 6200 3550 50  0000 L CNN
F 1 "SW_S" H 6150 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6150 3650 50  0001 C CNN
F 3 "" H 6150 3650 50  0001 C CNN
	1    6150 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW33
U 1 1 5A91EC78
P 6700 3450
F 0 "SW33" H 6750 3550 50  0000 L CNN
F 1 "SW_D" H 6700 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6700 3650 50  0001 C CNN
F 3 "" H 6700 3650 50  0001 C CNN
	1    6700 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW34
U 1 1 5A91EC8A
P 7250 3450
F 0 "SW34" H 7300 3550 50  0000 L CNN
F 1 "SW_F" H 7250 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7250 3650 50  0001 C CNN
F 3 "" H 7250 3650 50  0001 C CNN
	1    7250 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW35
U 1 1 5A91EC96
P 7800 3450
F 0 "SW35" H 7850 3550 50  0000 L CNN
F 1 "SW_G" H 7800 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7800 3650 50  0001 C CNN
F 3 "" H 7800 3650 50  0001 C CNN
	1    7800 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW36
U 1 1 5A91ECA2
P 8350 3450
F 0 "SW36" H 8400 3550 50  0000 L CNN
F 1 "SW_H" H 8350 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8350 3650 50  0001 C CNN
F 3 "" H 8350 3650 50  0001 C CNN
	1    8350 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW37
U 1 1 5A91ECAE
P 8900 3450
F 0 "SW37" H 8950 3550 50  0000 L CNN
F 1 "SW_J" H 8900 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8900 3650 50  0001 C CNN
F 3 "" H 8900 3650 50  0001 C CNN
	1    8900 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW38
U 1 1 5A91ECB4
P 9450 3450
F 0 "SW38" H 9500 3550 50  0000 L CNN
F 1 "SW_K" H 9450 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9450 3650 50  0001 C CNN
F 3 "" H 9450 3650 50  0001 C CNN
	1    9450 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW39
U 1 1 5A91ECBA
P 10000 3450
F 0 "SW39" H 10050 3550 50  0000 L CNN
F 1 "SW_L" H 10000 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10000 3650 50  0001 C CNN
F 3 "" H 10000 3650 50  0001 C CNN
	1    10000 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW40
U 1 1 5A91ECC0
P 10550 3450
F 0 "SW40" H 10600 3550 50  0000 L CNN
F 1 "SW_Fn" H 10550 3390 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10550 3650 50  0001 C CNN
F 3 "" H 10550 3650 50  0001 C CNN
	1    10550 3450
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW41
U 1 1 5A91F147
P 5600 3900
F 0 "SW41" H 5650 4000 50  0000 L CNN
F 1 "SW_Shift" H 5600 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5600 4100 50  0001 C CNN
F 3 "" H 5600 4100 50  0001 C CNN
	1    5600 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW42
U 1 1 5A91F153
P 6150 3900
F 0 "SW42" H 6200 4000 50  0000 L CNN
F 1 "SW_Z" H 6150 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6150 4100 50  0001 C CNN
F 3 "" H 6150 4100 50  0001 C CNN
	1    6150 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW43
U 1 1 5A91F159
P 6700 3900
F 0 "SW43" H 6750 4000 50  0000 L CNN
F 1 "SW_X" H 6700 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6700 4100 50  0001 C CNN
F 3 "" H 6700 4100 50  0001 C CNN
	1    6700 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW44
U 1 1 5A91F16B
P 7250 3900
F 0 "SW44" H 7300 4000 50  0000 L CNN
F 1 "SW_C" H 7250 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7250 4100 50  0001 C CNN
F 3 "" H 7250 4100 50  0001 C CNN
	1    7250 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW45
U 1 1 5A91F177
P 7800 3900
F 0 "SW45" H 7850 4000 50  0000 L CNN
F 1 "SW_V" H 7800 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7800 4100 50  0001 C CNN
F 3 "" H 7800 4100 50  0001 C CNN
	1    7800 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW46
U 1 1 5A91F183
P 8350 3900
F 0 "SW46" H 8400 4000 50  0000 L CNN
F 1 "SW_B" H 8350 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8350 4100 50  0001 C CNN
F 3 "" H 8350 4100 50  0001 C CNN
	1    8350 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW47
U 1 1 5A91F18F
P 8900 3900
F 0 "SW47" H 8950 4000 50  0000 L CNN
F 1 "SW_N" H 8900 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8900 4100 50  0001 C CNN
F 3 "" H 8900 4100 50  0001 C CNN
	1    8900 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW48
U 1 1 5A91F195
P 9450 3900
F 0 "SW48" H 9500 4000 50  0000 L CNN
F 1 "SW_M" H 9450 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9450 4100 50  0001 C CNN
F 3 "" H 9450 4100 50  0001 C CNN
	1    9450 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW49
U 1 1 5A91F19B
P 10000 3900
F 0 "SW49" H 10050 4000 50  0000 L CNN
F 1 "SW_Shift" H 10000 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10000 4100 50  0001 C CNN
F 3 "" H 10000 4100 50  0001 C CNN
	1    10000 3900
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW50
U 1 1 5A91F1A1
P 10550 3900
F 0 "SW50" H 10600 4000 50  0000 L CNN
F 1 "SW_BkSpace" H 10550 3840 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10550 4100 50  0001 C CNN
F 3 "" H 10550 4100 50  0001 C CNN
	1    10550 3900
	1    0    0    -1  
$EndComp
Text GLabel 5400 1500 1    60   Input ~ 0
COL0
Text GLabel 5950 1500 1    60   Input ~ 0
COL1
Text GLabel 6500 1500 1    60   Input ~ 0
COL2
Text GLabel 7050 1500 1    60   Input ~ 0
COL3
Text GLabel 7600 1500 1    60   Input ~ 0
COL4
Text GLabel 8150 1500 1    60   Input ~ 0
COL5
Text GLabel 8700 1500 1    60   Input ~ 0
COL6
Text GLabel 9250 1500 1    60   Input ~ 0
COL7
Text GLabel 9800 1500 1    60   Input ~ 0
COL8
Text GLabel 10350 1500 1    60   Input ~ 0
COL9
Text GLabel 5150 2300 0    60   Input ~ 0
ROW0
Text GLabel 5150 2750 0    60   Input ~ 0
ROW1
Text GLabel 5150 3200 0    60   Input ~ 0
ROW2
Text GLabel 5150 3650 0    60   Input ~ 0
ROW3
Text GLabel 5150 4100 0    60   Input ~ 0
ROW4
Text Notes 4800 1000 0    118  ~ 24
Keyboard Matrix
$Comp
L teensythumbboard-rescue:SW_Push SW51
U 1 1 5B47057F
P 5600 4400
F 0 "SW51" H 5650 4500 50  0000 L CNN
F 1 "SW_Tab" H 5600 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5600 4600 50  0001 C CNN
F 3 "" H 5600 4600 50  0001 C CNN
	1    5600 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW52
U 1 1 5B47058B
P 6150 4400
F 0 "SW52" H 6200 4500 50  0000 L CNN
F 1 "SW_Ctrl" H 6150 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6150 4600 50  0001 C CNN
F 3 "" H 6150 4600 50  0001 C CNN
	1    6150 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW53
U 1 1 5B470591
P 6700 4400
F 0 "SW53" H 6750 4500 50  0000 L CNN
F 1 "SW_Super" H 6700 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6700 4600 50  0001 C CNN
F 3 "" H 6700 4600 50  0001 C CNN
	1    6700 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW54
U 1 1 5B4705A3
P 7250 4400
F 0 "SW54" H 7300 4500 50  0000 L CNN
F 1 "SW_Alt" H 7250 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7250 4600 50  0001 C CNN
F 3 "" H 7250 4600 50  0001 C CNN
	1    7250 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW55
U 1 1 5B4705AF
P 7800 4400
F 0 "SW55" H 7850 4500 50  0000 L CNN
F 1 "SW_Space" H 7800 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7800 4600 50  0001 C CNN
F 3 "" H 7800 4600 50  0001 C CNN
	1    7800 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW56
U 1 1 5B4705BB
P 8350 4400
F 0 "SW56" H 8400 4500 50  0000 L CNN
F 1 "SW_Space" H 8350 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8350 4600 50  0001 C CNN
F 3 "" H 8350 4600 50  0001 C CNN
	1    8350 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW57
U 1 1 5B4705C7
P 8900 4400
F 0 "SW57" H 8950 4500 50  0000 L CNN
F 1 "SW_Fn" H 8900 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 8900 4600 50  0001 C CNN
F 3 "" H 8900 4600 50  0001 C CNN
	1    8900 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW58
U 1 1 5B4705CD
P 9450 4400
F 0 "SW58" H 9500 4500 50  0000 L CNN
F 1 "SW_," H 9450 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9450 4600 50  0001 C CNN
F 3 "" H 9450 4600 50  0001 C CNN
	1    9450 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW59
U 1 1 5B4705D3
P 10000 4400
F 0 "SW59" H 10050 4500 50  0000 L CNN
F 1 "SW_." H 10000 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10000 4600 50  0001 C CNN
F 3 "" H 10000 4600 50  0001 C CNN
	1    10000 4400
	1    0    0    -1  
$EndComp
$Comp
L teensythumbboard-rescue:SW_Push SW60
U 1 1 5B4705D9
P 10550 4400
F 0 "SW60" H 10600 4500 50  0000 L CNN
F 1 "SW_Ret" H 10550 4340 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10550 4600 50  0001 C CNN
F 3 "" H 10550 4600 50  0001 C CNN
	1    10550 4400
	1    0    0    -1  
$EndComp
Text GLabel 5150 4600 0    60   Input ~ 0
ROW5
Connection ~ 5400 3450
Connection ~ 5400 3000
Connection ~ 5400 2550
Wire Wire Line
	5950 1700 5950 2100
Connection ~ 5950 3450
Connection ~ 5950 3000
Connection ~ 5950 2550
Wire Wire Line
	6500 1700 6500 2100
Connection ~ 6500 3450
Connection ~ 6500 3000
Connection ~ 6500 2550
Wire Wire Line
	7050 1700 7050 2100
Connection ~ 7050 3450
Connection ~ 7050 3000
Connection ~ 7050 2550
Wire Wire Line
	7600 1700 7600 2100
Connection ~ 7600 3450
Connection ~ 7600 3000
Connection ~ 7600 2550
Wire Wire Line
	8150 1700 8150 2100
Connection ~ 8150 3450
Connection ~ 8150 3000
Connection ~ 8150 2550
Wire Wire Line
	8700 1700 8700 2100
Connection ~ 8700 3450
Connection ~ 8700 3000
Connection ~ 8700 2550
Wire Wire Line
	9250 1700 9250 2100
Connection ~ 9250 3450
Connection ~ 9250 3000
Connection ~ 9250 2550
Wire Wire Line
	9800 1700 9800 2100
Connection ~ 9800 3450
Connection ~ 9800 3000
Connection ~ 9800 2550
Wire Wire Line
	10350 1700 10350 2100
Connection ~ 10350 3450
Connection ~ 10350 3000
Connection ~ 10350 2550
Connection ~ 5950 2100
Connection ~ 6500 2100
Connection ~ 7050 2100
Connection ~ 7600 2100
Connection ~ 8150 2100
Connection ~ 8700 2100
Connection ~ 9250 2100
Connection ~ 9800 2100
Connection ~ 10350 2100
Wire Notes Line
	5450 800  5450 4950
Wire Notes Line
	4650 4850 11050 4850
Wire Notes Line
	11050 4850 11050 700 
Wire Notes Line
	11050 700  4650 700 
Connection ~ 10350 3900
Connection ~ 9800 3900
Connection ~ 9250 3900
Connection ~ 8700 3900
Connection ~ 8150 3900
Connection ~ 7600 3900
Connection ~ 7050 3900
Connection ~ 6500 3900
Connection ~ 5950 3900
Connection ~ 5400 3900
Text GLabel 2250 4450 0    60   Input ~ 0
ROW0
Text GLabel 2250 4550 0    60   Input ~ 0
ROW1
Text GLabel 2250 4750 0    60   Input ~ 0
ROW2
Text GLabel 2250 4850 0    60   Input ~ 0
ROW3
Text GLabel 2250 4650 0    60   Input ~ 0
ROW4
Text GLabel 2250 4950 0    60   Input ~ 0
ROW5
Wire Wire Line
	5400 3450 5400 3900
Wire Wire Line
	5400 3000 5400 3450
Wire Wire Line
	5400 2550 5400 3000
Wire Wire Line
	5950 3450 5950 3900
Wire Wire Line
	5950 3000 5950 3450
Wire Wire Line
	5950 2550 5950 3000
Wire Wire Line
	6500 3450 6500 3900
Wire Wire Line
	6500 3000 6500 3450
Wire Wire Line
	6500 2550 6500 3000
Wire Wire Line
	7050 3450 7050 3900
Wire Wire Line
	7050 3000 7050 3450
Wire Wire Line
	7050 2550 7050 3000
Wire Wire Line
	7600 3450 7600 3900
Wire Wire Line
	7600 3000 7600 3450
Wire Wire Line
	7600 2550 7600 3000
Wire Wire Line
	8150 3450 8150 3900
Wire Wire Line
	8150 3000 8150 3450
Wire Wire Line
	8150 2550 8150 3000
Wire Wire Line
	8700 3450 8700 3900
Wire Wire Line
	8700 3000 8700 3450
Wire Wire Line
	8700 2550 8700 3000
Wire Wire Line
	9250 3450 9250 3900
Wire Wire Line
	9250 3000 9250 3450
Wire Wire Line
	9250 2550 9250 3000
Wire Wire Line
	9800 3450 9800 3900
Wire Wire Line
	9800 3000 9800 3450
Wire Wire Line
	9800 2550 9800 3000
Wire Wire Line
	10350 3450 10350 3900
Wire Wire Line
	10350 3000 10350 3450
Wire Wire Line
	10350 2550 10350 3000
Wire Wire Line
	5950 2100 5950 2550
Wire Wire Line
	6500 2100 6500 2550
Wire Wire Line
	7050 2100 7050 2550
Wire Wire Line
	7600 2100 7600 2550
Wire Wire Line
	8150 2100 8150 2550
Wire Wire Line
	8700 2100 8700 2550
Wire Wire Line
	9250 2100 9250 2550
Wire Wire Line
	9800 2100 9800 2550
Wire Wire Line
	10350 2100 10350 2550
Wire Wire Line
	10350 3900 10350 4400
Wire Wire Line
	9800 3900 9800 4400
Wire Wire Line
	9250 3900 9250 4400
Wire Wire Line
	8700 3900 8700 4400
Wire Wire Line
	8150 3900 8150 4400
Wire Wire Line
	7600 3900 7600 4400
Wire Wire Line
	7050 3900 7050 4400
Wire Wire Line
	6500 3900 6500 4400
Wire Wire Line
	5950 3900 5950 4400
Wire Wire Line
	5400 3900 5400 4400
Text GLabel 2250 3450 0    60   Input ~ 0
COL0
Text GLabel 2250 3550 0    60   Input ~ 0
COL1
Text GLabel 2250 3650 0    60   Input ~ 0
COL2
Text GLabel 2250 3750 0    60   Input ~ 0
COL3
Text GLabel 2250 3850 0    60   Input ~ 0
COL4
Text GLabel 2250 3950 0    60   Input ~ 0
COL5
Text GLabel 2250 4050 0    60   Input ~ 0
COL6
Text GLabel 2250 4150 0    60   Input ~ 0
COL7
$Comp
L teensythumbboard-rescue:D_Zener_Small D10
U 1 1 5A91C5FC
P 10350 1600
F 0 "D10" H 10350 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 10350 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10350 1600 50  0001 C CNN
F 3 "" V 10350 1600 50  0001 C CNN
	1    10350 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D9
U 1 1 5A91C5A3
P 9800 1600
F 0 "D9" H 9800 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 9800 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9800 1600 50  0001 C CNN
F 3 "" V 9800 1600 50  0001 C CNN
	1    9800 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D8
U 1 1 5A91C549
P 9250 1600
F 0 "D8" H 9250 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 9250 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9250 1600 50  0001 C CNN
F 3 "" V 9250 1600 50  0001 C CNN
	1    9250 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D7
U 1 1 5A91C4F4
P 8700 1600
F 0 "D7" H 8700 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 8700 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8700 1600 50  0001 C CNN
F 3 "" V 8700 1600 50  0001 C CNN
	1    8700 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D6
U 1 1 5A91BCF2
P 8150 1600
F 0 "D6" H 8150 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 8150 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8150 1600 50  0001 C CNN
F 3 "" V 8150 1600 50  0001 C CNN
	1    8150 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D5
U 1 1 5A91BC63
P 7600 1600
F 0 "D5" H 7600 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 7600 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7600 1600 50  0001 C CNN
F 3 "" V 7600 1600 50  0001 C CNN
	1    7600 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D4
U 1 1 5A91BBE2
P 7050 1600
F 0 "D4" H 7050 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 7050 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7050 1600 50  0001 C CNN
F 3 "" V 7050 1600 50  0001 C CNN
	1    7050 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D1
U 1 1 5A91B709
P 5400 1600
F 0 "D1" H 5400 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 5400 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5400 1600 50  0001 C CNN
F 3 "" V 5400 1600 50  0001 C CNN
	1    5400 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D2
U 1 1 5A91BB48
P 5950 1600
F 0 "D2" H 5950 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 5950 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5950 1600 50  0001 C CNN
F 3 "" V 5950 1600 50  0001 C CNN
	1    5950 1600
	0    1    1    0   
$EndComp
$Comp
L teensythumbboard-rescue:D_Zener_Small D3
U 1 1 5A91BB7B
P 6500 1600
F 0 "D3" H 6500 1690 50  0000 C CNN
F 1 "D_Zener_Small" H 6500 1510 50  0001 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6500 1600 50  0001 C CNN
F 3 "" V 6500 1600 50  0001 C CNN
	1    6500 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 4600 5800 4600
Wire Wire Line
	5800 4400 5800 4600
Connection ~ 5800 4600
Wire Wire Line
	5800 4600 6350 4600
Wire Wire Line
	6350 4400 6350 4600
Connection ~ 6350 4600
Wire Wire Line
	6350 4600 6900 4600
Wire Wire Line
	6900 4400 6900 4600
Connection ~ 6900 4600
Wire Wire Line
	6900 4600 7450 4600
Wire Wire Line
	7450 4400 7450 4600
Connection ~ 7450 4600
Wire Wire Line
	7450 4600 8000 4600
Wire Wire Line
	8000 4400 8000 4600
Connection ~ 8000 4600
Wire Wire Line
	8000 4600 8550 4600
Wire Wire Line
	8550 4400 8550 4600
Connection ~ 8550 4600
Wire Wire Line
	8550 4600 9100 4600
Wire Wire Line
	9100 4400 9100 4600
Connection ~ 9100 4600
Wire Wire Line
	9100 4600 9650 4600
Wire Wire Line
	9650 4400 9650 4600
Connection ~ 9650 4600
Wire Wire Line
	9650 4600 10200 4600
Wire Wire Line
	10200 4400 10200 4600
Connection ~ 10200 4600
Wire Wire Line
	10200 4600 10750 4600
Wire Wire Line
	10750 4400 10750 4600
Wire Wire Line
	5800 2100 5800 2300
Connection ~ 5800 2300
Wire Wire Line
	6350 2100 6350 2300
Connection ~ 6350 2300
Wire Wire Line
	6350 2300 6900 2300
Wire Wire Line
	6900 2100 6900 2300
Connection ~ 6900 2300
Wire Wire Line
	6900 2300 7450 2300
Wire Wire Line
	7450 2100 7450 2300
Connection ~ 7450 2300
Wire Wire Line
	7450 2300 8000 2300
Wire Wire Line
	8000 2100 8000 2300
Connection ~ 8000 2300
Wire Wire Line
	8000 2300 8550 2300
Wire Wire Line
	8550 2100 8550 2300
Connection ~ 8550 2300
Wire Wire Line
	8550 2300 9100 2300
Wire Wire Line
	9100 2100 9100 2300
Connection ~ 9100 2300
Wire Wire Line
	9100 2300 9650 2300
Wire Wire Line
	9650 2100 9650 2300
Connection ~ 9650 2300
Wire Wire Line
	9650 2300 10200 2300
Wire Wire Line
	10200 2100 10200 2300
Connection ~ 10200 2300
Wire Wire Line
	10200 2300 10750 2300
Wire Wire Line
	10750 2100 10750 2300
Wire Wire Line
	5150 2750 5800 2750
Wire Wire Line
	5800 2550 5800 2750
Connection ~ 5800 2750
Wire Wire Line
	5800 2750 6350 2750
Wire Wire Line
	6350 2550 6350 2750
Connection ~ 6350 2750
Wire Wire Line
	6350 2750 6900 2750
Wire Wire Line
	6900 2550 6900 2750
Connection ~ 6900 2750
Wire Wire Line
	6900 2750 7450 2750
Wire Wire Line
	7450 2550 7450 2750
Connection ~ 7450 2750
Wire Wire Line
	7450 2750 8000 2750
Wire Wire Line
	8000 2550 8000 2750
Connection ~ 8000 2750
Wire Wire Line
	8000 2750 8550 2750
Wire Wire Line
	8550 2550 8550 2750
Connection ~ 8550 2750
Wire Wire Line
	8550 2750 9100 2750
Wire Wire Line
	9100 2550 9100 2750
Connection ~ 9100 2750
Wire Wire Line
	9100 2750 9650 2750
Wire Wire Line
	9650 2550 9650 2750
Connection ~ 9650 2750
Wire Wire Line
	9650 2750 10200 2750
Wire Wire Line
	10200 2550 10200 2750
Connection ~ 10200 2750
Wire Wire Line
	10200 2750 10750 2750
Wire Wire Line
	10750 2550 10750 2750
Wire Wire Line
	5150 3200 5800 3200
Wire Wire Line
	5800 3000 5800 3200
Connection ~ 5800 3200
Wire Wire Line
	5800 3200 6350 3200
Wire Wire Line
	6350 3000 6350 3200
Connection ~ 6350 3200
Wire Wire Line
	6350 3200 6900 3200
Wire Wire Line
	6900 3000 6900 3200
Connection ~ 6900 3200
Wire Wire Line
	6900 3200 7450 3200
Wire Wire Line
	7450 3000 7450 3200
Connection ~ 7450 3200
Wire Wire Line
	7450 3200 8000 3200
Wire Wire Line
	8000 3000 8000 3200
Connection ~ 8000 3200
Wire Wire Line
	8000 3200 8550 3200
Wire Wire Line
	8550 3000 8550 3200
Connection ~ 8550 3200
Wire Wire Line
	8550 3200 9100 3200
Wire Wire Line
	9100 3000 9100 3200
Connection ~ 9100 3200
Wire Wire Line
	9100 3200 9650 3200
Wire Wire Line
	9650 3000 9650 3200
Connection ~ 9650 3200
Wire Wire Line
	9650 3200 10200 3200
Wire Wire Line
	10200 3000 10200 3200
Connection ~ 10200 3200
Wire Wire Line
	10200 3200 10750 3200
Wire Wire Line
	10750 3000 10750 3200
Wire Wire Line
	5150 3650 5800 3650
Wire Wire Line
	5800 3450 5800 3650
Connection ~ 5800 3650
Wire Wire Line
	5800 3650 6350 3650
Wire Wire Line
	6350 3450 6350 3650
Connection ~ 6350 3650
Wire Wire Line
	6350 3650 6900 3650
Wire Wire Line
	6900 3450 6900 3650
Connection ~ 6900 3650
Wire Wire Line
	6900 3650 7450 3650
Wire Wire Line
	7450 3450 7450 3650
Connection ~ 7450 3650
Wire Wire Line
	7450 3650 8000 3650
Wire Wire Line
	8000 3450 8000 3650
Connection ~ 8000 3650
Wire Wire Line
	8000 3650 8550 3650
Wire Wire Line
	8550 3450 8550 3650
Connection ~ 8550 3650
Wire Wire Line
	8550 3650 9100 3650
Wire Wire Line
	9100 3450 9100 3650
Connection ~ 9100 3650
Wire Wire Line
	9100 3650 9650 3650
Wire Wire Line
	9650 3450 9650 3650
Connection ~ 9650 3650
Wire Wire Line
	9650 3650 10200 3650
Wire Wire Line
	10200 3450 10200 3650
Connection ~ 10200 3650
Wire Wire Line
	10200 3650 10750 3650
Wire Wire Line
	10750 3450 10750 3650
Wire Wire Line
	5150 4100 5800 4100
Wire Wire Line
	5800 3900 5800 4100
Connection ~ 5800 4100
Wire Wire Line
	5800 4100 6350 4100
Wire Wire Line
	6350 3900 6350 4100
Connection ~ 6350 4100
Wire Wire Line
	6350 4100 6900 4100
Wire Wire Line
	6900 3900 6900 4100
Connection ~ 6900 4100
Wire Wire Line
	6900 4100 7450 4100
Wire Wire Line
	7450 3900 7450 4100
Connection ~ 7450 4100
Wire Wire Line
	7450 4100 8000 4100
Wire Wire Line
	8000 3900 8000 4100
Connection ~ 8000 4100
Wire Wire Line
	8000 4100 8550 4100
Wire Wire Line
	8550 3900 8550 4100
Connection ~ 8550 4100
Wire Wire Line
	8550 4100 9100 4100
Wire Wire Line
	9100 3900 9100 4100
Connection ~ 9100 4100
Wire Wire Line
	9100 4100 9650 4100
Wire Wire Line
	9650 3900 9650 4100
Connection ~ 9650 4100
Wire Wire Line
	9650 4100 10200 4100
Wire Wire Line
	10200 3900 10200 4100
Connection ~ 10200 4100
Wire Wire Line
	10200 4100 10750 4100
Wire Wire Line
	10750 3900 10750 4100
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5DCAFCAA
P 1400 4050
F 0 "J1" H 1480 4042 50  0000 L CNN
F 1 "Internal USB" H 1480 3951 50  0000 L CNN
F 2 "Connector_FFC-FPC:Molex_200528-0040_1x04-1MP_P1.00mm_Horizontal" H 1400 4050 50  0001 C CNN
F 3 "~" H 1400 4050 50  0001 C CNN
	1    1400 4050
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Connectors:USB_MICRO-B_SMT J2
U 1 1 5DCB12D4
P 1350 3100
F 0 "J2" H 1350 3660 45  0000 C CNN
F 1 "Testing USB" H 1350 3576 45  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 1350 3550 20  0001 C CNN
F 3 "" H 1350 3100 50  0001 C CNN
F 4 "CONN-09505" H 1350 3481 60  0000 C CNN "Field4"
	1    1350 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 3950 750  3950
Wire Wire Line
	750  3950 750  2900
Wire Wire Line
	750  2900 1100 2900
Wire Wire Line
	1200 4050 800  4050
Wire Wire Line
	800  4050 800  3600
Wire Wire Line
	800  3000 1100 3000
Wire Wire Line
	1200 4150 850  4150
Wire Wire Line
	850  4150 850  3700
Wire Wire Line
	850  3100 1100 3100
Wire Wire Line
	1200 4250 900  4250
Wire Wire Line
	900  4250 900  3500
Wire Wire Line
	900  3300 1100 3300
Wire Wire Line
	900  3300 900  3200
Wire Wire Line
	900  3200 1100 3200
Connection ~ 900  3300
Wire Wire Line
	1600 3350 1600 3500
Wire Wire Line
	1600 3500 900  3500
Connection ~ 900  3500
Wire Wire Line
	900  3500 900  3300
$Comp
L power:+5V #PWR0101
U 1 1 5DBABEED
P 750 2400
F 0 "#PWR0101" H 750 2250 50  0001 C CNN
F 1 "+5V" H 765 2573 50  0000 C CNN
F 2 "" H 750 2400 50  0001 C CNN
F 3 "" H 750 2400 50  0001 C CNN
	1    750  2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5DBAEB3D
P 900 4450
F 0 "#PWR0102" H 900 4200 50  0001 C CNN
F 1 "GND" H 905 4277 50  0000 C CNN
F 2 "" H 900 4450 50  0001 C CNN
F 3 "" H 900 4450 50  0001 C CNN
	1    900  4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  4250 900  4450
Connection ~ 900  4250
Text GLabel 1200 3600 2    50   Input ~ 0
USB_D-
Text GLabel 1200 3700 2    50   Input ~ 0
USB_D+
Wire Wire Line
	1200 3600 800  3600
Connection ~ 800  3600
Wire Wire Line
	800  3600 800  3000
Wire Wire Line
	1200 3700 850  3700
Connection ~ 850  3700
Wire Wire Line
	850  3700 850  3100
Wire Wire Line
	1900 5850 1700 5850
$Comp
L power:GND #PWR0103
U 1 1 5DC2C354
P 4000 6500
F 0 "#PWR0103" H 4000 6250 50  0001 C CNN
F 1 "GND" H 4005 6327 50  0000 C CNN
F 2 "" H 4000 6500 50  0001 C CNN
F 3 "" H 4000 6500 50  0001 C CNN
	1    4000 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 6350 3300 6450
Wire Wire Line
	3300 6450 3400 6450
Wire Wire Line
	4000 6450 4000 6500
Wire Wire Line
	3400 6350 3400 6450
Connection ~ 3400 6450
Wire Wire Line
	3400 6450 3500 6450
Wire Wire Line
	3500 6350 3500 6450
Connection ~ 3500 6450
Wire Wire Line
	3500 6450 3600 6450
Wire Wire Line
	3600 6350 3600 6450
Connection ~ 3600 6450
Wire Wire Line
	3600 6450 3700 6450
Wire Wire Line
	3700 6350 3700 6450
Connection ~ 3700 6450
Wire Wire Line
	3700 6450 4000 6450
Text GLabel 2250 3150 0    50   Input ~ 0
USB_D+
$Comp
L Device:R_Small_US R4
U 1 1 5DC7AFF1
P 2500 3150
F 0 "R4" V 2600 2850 50  0000 C CNN
F 1 "22" V 2600 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2500 3150 50  0001 C CNN
F 3 "~" H 2500 3150 50  0001 C CNN
	1    2500 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R3
U 1 1 5DC7DC51
P 2500 3050
F 0 "R3" V 2295 3050 50  0000 C CNN
F 1 "22" V 2386 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2500 3050 50  0001 C CNN
F 3 "~" H 2500 3050 50  0001 C CNN
	1    2500 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	2600 3150 2800 3150
Wire Wire Line
	2250 3150 2400 3150
Wire Wire Line
	2600 3050 2800 3050
Text GLabel 2250 3050 0    50   Input ~ 0
USB_D-
Wire Wire Line
	2250 3050 2400 3050
Wire Wire Line
	2800 3450 2250 3450
Wire Wire Line
	2250 3550 2800 3550
Wire Wire Line
	2800 3650 2250 3650
Wire Wire Line
	2250 3750 2800 3750
Wire Wire Line
	2800 3850 2250 3850
Wire Wire Line
	2250 3950 2800 3950
Wire Wire Line
	2800 4050 2250 4050
Wire Wire Line
	2250 4150 2800 4150
Wire Wire Line
	2250 4250 2800 4250
Wire Wire Line
	2800 4350 2250 4350
Wire Wire Line
	2250 4450 2800 4450
Wire Wire Line
	2800 4550 2250 4550
Wire Wire Line
	2250 4650 2800 4650
Wire Wire Line
	2250 4750 2800 4750
Wire Wire Line
	3300 2850 3300 2700
Wire Wire Line
	3300 2700 3400 2700
Wire Wire Line
	3700 2700 3700 2850
Wire Wire Line
	3600 2850 3600 2700
Connection ~ 3600 2700
Wire Wire Line
	3600 2700 3700 2700
Wire Wire Line
	3500 2850 3500 2700
Connection ~ 3500 2700
Wire Wire Line
	3500 2700 3600 2700
Wire Wire Line
	3400 2850 3400 2700
Connection ~ 3400 2700
Wire Wire Line
	3400 2700 3500 2700
Wire Wire Line
	3500 2550 3500 2700
Wire Wire Line
	2800 5850 2500 5850
Wire Wire Line
	2500 5850 2500 5750
$Comp
L Device:R_Small_US R2
U 1 1 5DED3AD6
P 6500 6100
F 0 "R2" V 6295 6100 50  0000 C CNN
F 1 "10K" V 6386 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6500 6100 50  0001 C CNN
F 3 "~" H 6500 6100 50  0001 C CNN
	1    6500 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5900 6500 6000
$Comp
L power:GND #PWR0106
U 1 1 5DF0EA1D
P 4050 6150
F 0 "#PWR0106" H 4050 5900 50  0001 C CNN
F 1 "GND" H 4055 5977 50  0000 C CNN
F 2 "" H 4050 6150 50  0001 C CNN
F 3 "" H 4050 6150 50  0001 C CNN
	1    4050 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5DF51C99
P 4050 6050
F 0 "C1" H 4142 6096 50  0000 L CNN
F 1 "10uF" H 4142 6005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 4050 6050 50  0001 C CNN
F 3 "~" H 4050 6050 50  0001 C CNN
	1    4050 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 5850 4050 5950
Wire Wire Line
	3900 5850 4050 5850
NoConn ~ 2800 5050
NoConn ~ 2800 5150
NoConn ~ 2800 5250
NoConn ~ 2800 5350
NoConn ~ 2800 5450
NoConn ~ 2800 5550
NoConn ~ 2800 5650
NoConn ~ 2800 5750
NoConn ~ 2800 3250
NoConn ~ 2800 3350
Text GLabel 2250 4250 0    60   Input ~ 0
COL8
Text GLabel 2250 4350 0    60   Input ~ 0
COL9
Wire Wire Line
	2250 4850 2800 4850
Wire Wire Line
	2250 4950 2800 4950
Text GLabel 2700 5950 0    50   Input ~ 0
RST
Wire Wire Line
	2800 5950 2700 5950
Text GLabel 6400 6350 0    50   Input ~ 0
RST
Wire Wire Line
	6400 6350 6500 6350
Wire Wire Line
	6500 6350 6500 6200
$Comp
L Connector:TestPoint TP0
U 1 1 5DDE7336
P 6500 6350
F 0 "TP0" V 6454 6538 50  0000 L CNN
F 1 "TP_RST" V 6545 6538 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 6700 6350 50  0001 C CNN
F 3 "~" H 6700 6350 50  0001 C CNN
	1    6500 6350
	0    1    1    0   
$EndComp
Connection ~ 6500 6350
$Comp
L Connector:TestPoint TP1
U 1 1 5DDE7D13
P 7350 6350
F 0 "TP1" V 7545 6422 50  0000 C CNN
F 1 "TP_GND" V 7454 6422 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 7550 6350 50  0001 C CNN
F 3 "~" H 7550 6350 50  0001 C CNN
	1    7350 6350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5DDEE6CE
P 7700 6350
F 0 "#PWR0108" H 7700 6100 50  0001 C CNN
F 1 "GND" V 7705 6222 50  0000 R CNN
F 2 "" H 7700 6350 50  0001 C CNN
F 3 "" H 7700 6350 50  0001 C CNN
	1    7700 6350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 6350 7700 6350
$Comp
L Connector:TestPoint TP3
U 1 1 5DE13F45
P 5300 6200
F 0 "TP3" H 5358 6318 50  0000 L CNN
F 1 "TP_5V" H 5358 6227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 5500 6200 50  0001 C CNN
F 3 "~" H 5500 6200 50  0001 C CNN
	1    5300 6200
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 5DE343F2
P 5300 5900
F 0 "#PWR0109" H 5300 5750 50  0001 C CNN
F 1 "+5V" H 5315 6073 50  0000 C CNN
F 2 "" H 5300 5900 50  0001 C CNN
F 3 "" H 5300 5900 50  0001 C CNN
	1    5300 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 5900 5300 6200
$Comp
L Device:LED_Small D11
U 1 1 5DE6B8F0
P 5800 6900
F 0 "D11" H 5800 6695 50  0000 C CNN
F 1 "ON" H 5800 6786 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5800 6900 50  0001 C CNN
F 3 "~" V 5800 6900 50  0001 C CNN
	1    5800 6900
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R5
U 1 1 5DE6D970
P 6000 7100
F 0 "R5" H 6068 7146 50  0000 L CNN
F 1 "10K" H 6068 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6000 7100 50  0001 C CNN
F 3 "~" H 6000 7100 50  0001 C CNN
	1    6000 7100
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5DE6E456
P 5500 6800
F 0 "#PWR0110" H 5500 6650 50  0001 C CNN
F 1 "+5V" H 5515 6973 50  0000 C CNN
F 2 "" H 5500 6800 50  0001 C CNN
F 3 "" H 5500 6800 50  0001 C CNN
	1    5500 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5DE6ED13
P 6000 7350
F 0 "#PWR0111" H 6000 7100 50  0001 C CNN
F 1 "GND" H 6005 7177 50  0000 C CNN
F 2 "" H 6000 7350 50  0001 C CNN
F 3 "" H 6000 7350 50  0001 C CNN
	1    6000 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 6800 5500 6900
Wire Wire Line
	5500 6900 5700 6900
Wire Wire Line
	5900 6900 6000 6900
Wire Wire Line
	6000 6900 6000 7000
Wire Wire Line
	6000 7200 6000 7350
$Comp
L Oscillator:ECS-2520MV-xxx-xx X1
U 1 1 5DF254C7
P 1250 5850
F 0 "X1" H 1050 6100 50  0000 L CNN
F 1 "16MHz" H 1300 6100 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_ECS_2520MV-xxx-xx-4Pin_2.5x2.0mm" H 1700 5500 50  0001 C CNN
F 3 "https://www.ecsxtal.com/store/pdf/ECS-2520MV.pdf" H 1075 5975 50  0001 C CNN
	1    1250 5850
	1    0    0    -1  
$EndComp
NoConn ~ 3900 3550
$Comp
L power:GND #PWR0113
U 1 1 5DF8E088
P 1250 6300
F 0 "#PWR0113" H 1250 6050 50  0001 C CNN
F 1 "GND" H 1255 6127 50  0000 C CNN
F 2 "" H 1250 6300 50  0001 C CNN
F 3 "" H 1250 6300 50  0001 C CNN
	1    1250 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 5550 1250 5400
Wire Wire Line
	1250 6150 1250 6300
NoConn ~ 850  5850
$Comp
L Connector:TestPoint TP4
U 1 1 5DFCF7B1
P 1700 5850
F 0 "TP4" H 1642 5876 50  0000 R CNN
F 1 "TP_CLK" H 1642 5967 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 1900 5850 50  0001 C CNN
F 3 "~" H 1900 5850 50  0001 C CNN
	1    1700 5850
	-1   0    0    1   
$EndComp
Connection ~ 1700 5850
Wire Wire Line
	1700 5850 1650 5850
$Comp
L dk_Embedded-Microcontrollers:ATMEGA32U4-AU U1
U 1 1 5DD1DFAA
P 3400 4350
F 0 "U1" H 3944 4153 60  0000 L CNN
F 1 "ATMEGA32U4-AU" H 3944 4047 60  0000 L CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 3600 4550 60  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Summary.pdf" H 3600 4650 60  0001 L CNN
F 4 "ATMEGA32U4-AU-ND" H 3600 4750 60  0001 L CNN "Digi-Key_PN"
F 5 "ATMEGA32U4-AU" H 3600 4850 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 3600 4950 60  0001 L CNN "Category"
F 7 "Embedded - Microcontrollers" H 3600 5050 60  0001 L CNN "Family"
F 8 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Summary.pdf" H 3600 5150 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/ATMEGA32U4-AU/ATMEGA32U4-AU-ND/1914602" H 3600 5250 60  0001 L CNN "DK_Detail_Page"
F 10 "IC MCU 8BIT 32KB FLASH 44TQFP" H 3600 5350 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 3600 5450 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3600 5550 60  0001 L CNN "Status"
	1    3400 4350
	1    0    0    -1  
$EndComp
NoConn ~ 2800 6150
Wire Wire Line
	5150 2300 5800 2300
Wire Wire Line
	5400 2100 5400 2550
Text GLabel 1900 5850 2    50   Input ~ 0
CLK
Text GLabel 2800 6050 0    50   Input ~ 0
CLK
$Comp
L dk_PMIC-Voltage-Regulators-Linear:MCP1700T-3302E_TT U2
U 1 1 5DE86F9B
P 2500 6950
F 0 "U2" H 2500 7237 60  0000 C CNN
F 1 "MCP1700T-3302E_TT" H 2500 7131 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2700 7150 60  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 2700 7250 60  0001 L CNN
F 4 "MCP1700T3302ETTCT-ND" H 2700 7350 60  0001 L CNN "Digi-Key_PN"
F 5 "MCP1700T-3302E/TT" H 2700 7450 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 2700 7550 60  0001 L CNN "Category"
F 7 "PMIC - Voltage Regulators - Linear" H 2700 7650 60  0001 L CNN "Family"
F 8 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011779" H 2700 7750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/microchip-technology/MCP1700T-3302E-TT/MCP1700T3302ETTCT-ND/652677" H 2700 7850 60  0001 L CNN "DK_Detail_Page"
F 10 "IC REG LINEAR 3.3V 250MA SOT23-3" H 2700 7950 60  0001 L CNN "Description"
F 11 "Microchip Technology" H 2700 8050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 2700 8150 60  0001 L CNN "Status"
	1    2500 6950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 5DE89CCD
P 1950 6800
F 0 "#PWR0112" H 1950 6650 50  0001 C CNN
F 1 "+5V" H 1965 6973 50  0000 C CNN
F 2 "" H 1950 6800 50  0001 C CNN
F 3 "" H 1950 6800 50  0001 C CNN
	1    1950 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5DE8A77A
P 1950 7150
F 0 "C2" H 2042 7196 50  0000 L CNN
F 1 "1uF" H 2042 7105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1950 7150 50  0001 C CNN
F 3 "~" H 1950 7150 50  0001 C CNN
	1    1950 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5DE8B8F9
P 3000 7150
F 0 "C3" H 3092 7196 50  0000 L CNN
F 1 "1uF" H 3092 7105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3000 7150 50  0001 C CNN
F 3 "~" H 3000 7150 50  0001 C CNN
	1    3000 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 6800 1950 6950
Wire Wire Line
	2200 6950 1950 6950
Connection ~ 1950 6950
Wire Wire Line
	1950 6950 1950 7050
Wire Wire Line
	2800 6950 3000 6950
Wire Wire Line
	3000 6950 3000 7050
$Comp
L power:+3.3V #PWR0114
U 1 1 5DECEC15
P 3000 6800
F 0 "#PWR0114" H 3000 6650 50  0001 C CNN
F 1 "+3.3V" H 3015 6973 50  0000 C CNN
F 2 "" H 3000 6800 50  0001 C CNN
F 3 "" H 3000 6800 50  0001 C CNN
	1    3000 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 6800 3000 6950
Connection ~ 3000 6950
$Comp
L power:GND #PWR0115
U 1 1 5DEE6784
P 2500 7450
F 0 "#PWR0115" H 2500 7200 50  0001 C CNN
F 1 "GND" H 2505 7277 50  0000 C CNN
F 2 "" H 2500 7450 50  0001 C CNN
F 3 "" H 2500 7450 50  0001 C CNN
	1    2500 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 7250 3000 7350
Wire Wire Line
	3000 7350 2500 7350
Wire Wire Line
	2500 7350 2500 7450
Wire Wire Line
	2500 7350 1950 7350
Wire Wire Line
	1950 7350 1950 7250
Connection ~ 2500 7350
Wire Wire Line
	2500 7250 2500 7350
$Comp
L power:+3.3V #PWR0116
U 1 1 5DF4B9DB
P 1250 5400
F 0 "#PWR0116" H 1250 5250 50  0001 C CNN
F 1 "+3.3V" H 1265 5573 50  0000 C CNN
F 2 "" H 1250 5400 50  0001 C CNN
F 3 "" H 1250 5400 50  0001 C CNN
	1    1250 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2300 6350 2300
Wire Wire Line
	5400 1700 5400 2100
Connection ~ 5400 2100
$Comp
L Connector:TestPoint TP5
U 1 1 5E028BE1
P 5100 7350
F 0 "TP5" H 5042 7376 50  0000 R CNN
F 1 "TP_3.3" H 5042 7467 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 5300 7350 50  0001 C CNN
F 3 "~" H 5300 7350 50  0001 C CNN
	1    5100 7350
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR0117
U 1 1 5E029694
P 5100 6850
F 0 "#PWR0117" H 5100 6700 50  0001 C CNN
F 1 "+3.3V" H 5115 7023 50  0000 C CNN
F 2 "" H 5100 6850 50  0001 C CNN
F 3 "" H 5100 6850 50  0001 C CNN
	1    5100 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 6850 5100 7350
$Comp
L Device:Fuse_Small F0
U 1 1 5E102189
P 750 2650
F 0 "F0" V 704 2698 50  0000 L CNN
F 1 "Fuse_Small" V 795 2698 50  0000 L CNN
F 2 "Fuse:Fuse_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 750 2650 50  0001 C CNN
F 3 "~" H 750 2650 50  0001 C CNN
	1    750  2650
	0    1    1    0   
$EndComp
Wire Wire Line
	750  2750 750  2900
Connection ~ 750  2900
Wire Wire Line
	750  2550 750  2400
$Comp
L Connector:TestPoint TP6
U 1 1 5DE82424
P 700 3500
F 0 "TP6" H 758 3618 50  0000 L CNN
F 1 "USB_D-" H 758 3527 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 900 3500 50  0001 C CNN
F 3 "~" H 900 3500 50  0001 C CNN
	1    700  3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5DE82AFE
P 550 3600
F 0 "TP7" H 608 3718 50  0000 L CNN
F 1 "USB_D+" H 608 3627 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 750 3600 50  0001 C CNN
F 3 "~" H 750 3600 50  0001 C CNN
	1    550  3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  3600 700  3600
Wire Wire Line
	700  3600 700  3500
Wire Wire Line
	850  3700 550  3700
Wire Wire Line
	550  3700 550  3600
$Comp
L power:+3.3V #PWR0104
U 1 1 5DEDDF73
P 3600 850
F 0 "#PWR0104" H 3600 700 50  0001 C CNN
F 1 "+3.3V" H 3615 1023 50  0000 C CNN
F 2 "" H 3600 850 50  0001 C CNN
F 3 "" H 3600 850 50  0001 C CNN
	1    3600 850 
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0105
U 1 1 5DF568DC
P 2500 5750
F 0 "#PWR0105" H 2500 5600 50  0001 C CNN
F 1 "+3.3V" H 2515 5923 50  0000 C CNN
F 2 "" H 2500 5750 50  0001 C CNN
F 3 "" H 2500 5750 50  0001 C CNN
	1    2500 5750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0107
U 1 1 5DF81D81
P 6500 5900
F 0 "#PWR0107" H 6500 5750 50  0001 C CNN
F 1 "+3.3V" H 6515 6073 50  0000 C CNN
F 2 "" H 6500 5900 50  0001 C CNN
F 3 "" H 6500 5900 50  0001 C CNN
	1    6500 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5DFAF9B6
P 3700 2350
F 0 "C7" V 3471 2350 50  0000 C CNN
F 1 "1uF" V 3562 2350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3700 2350 50  0001 C CNN
F 3 "~" H 3700 2350 50  0001 C CNN
	1    3700 2350
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5DFB0A05
P 3700 2000
F 0 "C6" V 3471 2000 50  0000 C CNN
F 1 "1uF" V 3562 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3700 2000 50  0001 C CNN
F 3 "~" H 3700 2000 50  0001 C CNN
	1    3700 2000
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5DFB18A2
P 3700 1600
F 0 "C5" V 3471 1600 50  0000 C CNN
F 1 "1uF" V 3562 1600 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3700 1600 50  0001 C CNN
F 3 "~" H 3700 1600 50  0001 C CNN
	1    3700 1600
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5DFB28DB
P 3700 1250
F 0 "C4" V 3471 1250 50  0000 C CNN
F 1 "1uF" V 3562 1250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3700 1250 50  0001 C CNN
F 3 "~" H 3700 1250 50  0001 C CNN
	1    3700 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	3600 850  3600 1250
Wire Wire Line
	3600 1250 3600 1600
Connection ~ 3600 1250
Wire Wire Line
	3600 1600 3600 2000
Connection ~ 3600 1600
Wire Wire Line
	3600 2000 3600 2350
Connection ~ 3600 2000
Wire Wire Line
	3600 2350 3600 2550
Wire Wire Line
	3600 2550 3500 2550
Connection ~ 3600 2350
$Comp
L power:GND #PWR0118
U 1 1 5E058B2F
P 4000 2550
F 0 "#PWR0118" H 4000 2300 50  0001 C CNN
F 1 "GND" H 4005 2377 50  0000 C CNN
F 2 "" H 4000 2550 50  0001 C CNN
F 3 "" H 4000 2550 50  0001 C CNN
	1    4000 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1250 4000 1250
Wire Wire Line
	4000 1250 4000 1600
Wire Wire Line
	3800 1600 4000 1600
Connection ~ 4000 1600
Wire Wire Line
	4000 1600 4000 2000
Wire Wire Line
	3800 2000 4000 2000
Connection ~ 4000 2000
Wire Wire Line
	4000 2000 4000 2350
Wire Wire Line
	3800 2350 4000 2350
Connection ~ 4000 2350
Wire Wire Line
	4000 2350 4000 2550
$EndSCHEMATC
